#!/usr/bin/env powershell

#This will ask for login credentials the first time it's run on a new machine, and then store them in Secure folder
function Get-Five9Credentials ($domain, $Dir){
    #Path to stored credential
    $credPath = "$Dir\Secure\Cred_$Domain.xml"
    #Check for stored credential
    if (Test-Path $credPath) {
        #crendetial is stored, load it 
        $cred = Import-CliXml -Path $credPath
        return $cred
    } 
    else {
        # no stored credential: create store, get credential and save it
        $parent = split-path $credpath -parent
        if ( -not ( test-Path $parent ) ) {
            New-Item -ItemType Directory -Force -Path $parent
        }
        $cred = get-credential
        $cred | Export-CliXml -Path $credPath
        return $cred
    }
}