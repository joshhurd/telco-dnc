#!/usr/bin/env powershell

#Functions imported from PSFive9Admin https://github.com/sqone2/PSFive9Admin
#Connect to Five9 domain using credentials
function Connect-Five9AdminWebService {
    [CmdletBinding(PositionalBinding=$true)]

    param (
        [Parameter(Mandatory=$false)][PSCredential]$Credential = (Get-Credential),
        [Parameter(Mandatory=$false)][string]$Version = '11',
        [Parameter(Mandatory=$false)][switch]$PassThru = $false
    )

    try {
        try {
            [System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
        }
        catch {}

        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

        $wsdl = "https://api.five9.com/wsadmin/v$($Version)/AdminWebService?wsdl&user=$($Credential.Username)"
        Write-Verbose "Connecting to: $($wsdl)"

        $global:DefaultFive9AdminClient = New-WebServiceProxy -Uri $wsdl -Namespace "PSFive9Admin" -Class "PSFive9Admin" -ErrorAction: Stop

        $global:DefaultFive9AdminClient.Credentials = $Credential

        $global:DefaultFive9AdminClient | Add-Member -MemberType NoteProperty -Name Five9DomainName -Value $null -Force
        $global:DefaultFive9AdminClient | Add-Member -MemberType NoteProperty -Name Five9DomainId -Value $null -Force
    }

    catch {
        throw "Error creating web service proxy to Five9 admin web service. $($_.Exception.Message)"
        return
    }
    
    #Test credentials
    try {
        $vccConfig = $global:DefaultFive9AdminClient.getVCCConfiguration()
        Write-Verbose "Connection established to domain id $($vccConfig.domainId) ($($vccConfig.domainName))"

        $global:DefaultFive9AdminClient.Five9DomainName = $vccConfig.domainName
        $global:DefaultFive9AdminClient.Five9DomainId = $vccConfig.domainId

    }
    catch {
        throw "Error connecting to Five9 admin web service. Please check your credentials and try again. $($_.Exception.Message)"
        return
    }

    if ($PassThru -eq $true) {
        return $global:DefaultFive9AdminClient
    }
    return
}

#Add DNC Number
function Add-Five9DNCNumber{
    [CmdletBinding(PositionalBinding=$true)]
    param( 
        # One or more numbers to be added to the DNC list
        [Parameter(Mandatory=$true)][string[]]$Number,
        [Parameter(Mandatory=$true)][string[]]$Domain
    )
    
    try {
        Test-Five9Connection -ErrorAction: Stop

        if ($Number.Count -eq 1) {
            Write-Verbose "$($MyInvocation.MyCommand.Name): Adding '$Number' to the DNC list in $Domain."
        }
        else{
            Write-Verbose "$($MyInvocation.MyCommand.Name): Adding $($Number.Count) numbers to the DNC list in $Domain."
        }
        
        return $global:DefaultFive9AdminClient.addNumbersToDnc($Number)
    }

    catch{
        $_ | Write-PSFive9AdminError
        $_ | Write-Error
    }
}

#Check if DNC number is added
function Get-Five9DNCNumber {
    [CmdletBinding(PositionalBinding=$true)]
    param ( 
        # One or more numbers to search for in the DNC list
        # You may include up to 50000 phone numbers in a request
        [Parameter(Mandatory=$true)][string[]]$Number,
        [Parameter(Mandatory=$true)][string[]]$Domain
    )
    
    try {
Add-Type @"
public struct dncNumber {
    public string number;
    public bool DNC;
}
"@ -IgnoreWarnings

        Test-Five9Connection -ErrorAction: Stop

        if ($Number.Count -eq 1) {
            Write-Verbose "$($MyInvocation.MyCommand.Name): Checking DNC for '$Number' in $Domain."

            $response = $global:DefaultFive9AdminClient.checkDncForNumbers($Number)

            if ($null -eq $response) {
                return $false
            }
            else {
                return $true
            }

        }
        else {
            Write-Verbose "$($MyInvocation.MyCommand.Name): Checking DNC for $($Number.Count) numbers in $Domain." 

            $response = $global:DefaultFive9AdminClient.checkDncForNumbers($Number)
            
            $returnList = @()
            foreach ($num in $Number) {
                if ($num.Length -gt 5) {
                    $dncNumber = New-Object -TypeName dncNumber
                    $dncNumber.number = $num
                    [bool]$dncNumber.DNC = $($response -contains $num)

                    $returnList += $dncNumber
                }
            }
            return $returnList | Sort-Object number
        }
    }
    catch {
        $_ | Write-PSFive9AdminError
        $_ | Write-Error
    }
}

function Test-Five9Connection {
    [CmdletBinding(PositionalBinding=$false)]
    param ()
    if ($global:DefaultFive9AdminClient.Five9DomainName.Length -gt 0) {
        return
    }
    throw "You are not currently connected to the Five9 Admin Web Service. You must first connect using Connect-Five9AdminWebService."
    return
}

#Remove number from DNC
function Remove-Five9DNCNumber {
    [CmdletBinding(PositionalBinding=$true)]
    param ( 
        # One or more numbers to be removed from the DNC list
        [Parameter(Mandatory=$true)][string[]]$Number
    )
    
    try {
        Test-Five9Connection -ErrorAction: Stop

        if ($Number.Count -eq 1) {
            Write-Verbose "$($MyInvocation.MyCommand.Name): Removing '$Number' from the DNC list."
        }
        else {
            Write-Verbose "$($MyInvocation.MyCommand.Name): Removing $($Number.Count) numbers from the DNC list."
        }
        return  $global:DefaultFive9AdminClient.removeNumbersFromDnc($Number)
    }
    catch {
        $_ | Write-PSFive9AdminError
        $_ | Write-Error
    }
}

function Write-PSFive9AdminError {
    [CmdletBinding(PositionalBinding=$false)]

    param (
        [Parameter(Mandatory=$true,ValueFromPipeline=$true,Position=0)][object]$Exception
    )
    if ($Exception.Exception.Message -match 'You are not currently connected to the Five9 Admin Web Service') {
        throw $Exception
    }
    elseif ($Exception.Exception.Message -match 'You are not currently connected to the Five9 Statistics Web Service') {
        throw $Exception
    }
    elseif ($Exception.Message -match 'Session was closed') {
        throw "Your statistics session has expired. Please reconnect using Connect-Five9Statistics."
    }
}

#Add option for silent or verbose output
function Get-AllFive9DNC {
    param(
        $numbers,
        [PSCredential]$SeniorCreds,
        [PSCredential]$UHCCreds,
        [PSCredential]$CCACreds,
        [PSCredential]$SQAHCreds,
        [PSCredential]$HealthCreds,
        [PSCredential]$LifeCreds
    )

    $numberArray = New-Object System.Collections.ArrayList($null)

    #Get numbers from Senior
    Connect-Five9AdminWebService -Credential $SeniorCreds
    $matchedSenior = Get-Five9DNCNumber $numbers "Senior" -Verbose
    Write-Host ($matchedSenior | Out-String)
    ForEach ($item in $matchedSenior) {
        if ($item.DNC) {
            $numberArray.add([PSCustomObject] @{
                Number = $item.number
                Domain = "Senior"
            }) | Out-Null
        }
        elseif ($item -eq $True){
            $numberArray.add([PSCustomObject] @{
                Number = $numbers
                Domain = "Senior"
            }) | Out-Null
        }
    }
    #Get numbers from UHC
    Connect-Five9AdminWebService -Credential $UHCCreds
    $matchedUHC = Get-Five9DNCNumber $numbers "UHC" -Verbose
    Write-Host ($matchedUHC | Out-String)
    ForEach ($item in $matchedUHC) {
        if ($item.DNC) {
            $numberArray.add([PSCustomObject] @{
                Number = $item.number
                Domain = "UHC"
            }) | Out-Null
        }
        elseif ($item -eq $True){
            $numberArray.add([PSCustomObject] @{
                Number = $numbers
                Domain = "UHC"
            }) | Out-Null
        }
    }
    #Get numbers from CCA
    Connect-Five9AdminWebService -Credential $CCACreds
    $matchedCCA = Get-Five9DNCNumber $numbers "CCA" -Verbose
    Write-Host ($matchedCCA | Out-String)
    ForEach ($item in $matchedCCA) {
        if ($item.DNC) {
            $numberArray.add([PSCustomObject] @{
                Number = $item.number
                Domain = "CCA"
            }) | Out-Null
        }
        elseif ($item -eq $True){
            $numberArray.add([PSCustomObject] @{
                Number = $numbers
                Domain = "CCA"
            }) | Out-Null
        }
    }
    #Get numbers from SQAH
    Connect-Five9AdminWebService -Credential $SQAHCreds
    $matchedSQAH = Get-Five9DNCNumber $numbers "SQAH" -Verbose
    Write-Host ($matchedSQAH | Out-String)
    ForEach ($item in $matchedSQAH) {
        if ($item.DNC) {
            $numberArray.add([PSCustomObject] @{
                Number = $item.number
                Domain = "SQAH"
            }) | Out-Null
        }
        elseif ($item -eq $True){
            $numberArray.add([PSCustomObject] @{
                Number = $numbers
                Domain = "SQAH"
            }) | Out-Null
        }
    }
    #Get numbers from Health
    Connect-Five9AdminWebService -Credential $HealthCreds
    $matchedHealth = Get-Five9DNCNumber $numbers "Health" -Verbose
    Write-Host ($matchedHealth | Out-String)
    ForEach ($item in $matchedHealth) {
        if ($item.DNC) {
            $numberArray.add([PSCustomObject] @{
                Number = $item.number
                Domain = "Health"
            }) | Out-Null
        }
        elseif ($item -eq $True){
            $numberArray.add([PSCustomObject] @{
                Number = $numbers
                Domain = "Health"
            }) | Out-Null
        }
    }
    #Get numbers from Life
    Connect-Five9AdminWebService -Credential $LifeCreds
    $matchedLife = Get-Five9DNCNumber $numbers "Life" -Verbose
    Write-Host ($matchedLife | Out-String)
    ForEach ($item in $matchedLife) {
        if ($item.DNC) {
            $numberArray.add([PSCustomObject] @{
                Number = $item.number
                Domain = "Life"
            }) | Out-Null
        }
        elseif ($item -eq $True){
            $numberArray.add([PSCustomObject] @{
                Number = $numbers
                Domain = "Life"
            }) | Out-Null
        }
    }
    return $numberArray
}


#Add number to all domains if it's not already in the domain
function Add-DNCAllDomains ($number, $matched) {
    $SeniorNum = ($matched | Where-Object {$_.Domain -eq "Senior"})
    $UHCNum = ($matched| Where-Object {$_.Domain -eq "UHC"})
    $CCANum = ($matched | Where-Object {$_.Domain -eq "CCA"})
    $SQAHNum = ($matched | Where-Object {$_.Domain -eq "SQAH"})
    $HealthNum = ($matched | Where-Object {$_.Domain -eq "Health"})
    $LifeNum = ($matched | Where-Object {$_.Domain -eq "Life"})
    if ($SeniorNum.Number -notcontains $number) {
        Connect-Five9AdminWebService -Credential $SeniorCreds
        Add-Five9DNCNumber $number "Senior" -Verbose
    }
    if ($CCANum.Number -notcontains $number) {
        Connect-Five9AdminWebService -Credential $CCACreds
        Add-Five9DNCNumber $number "UHC" -Verbose
    }
    if ($UHCNum.Number -notcontains $number) {
        Connect-Five9AdminWebService -Credential $UHCCreds
        Add-Five9DNCNumber $number "CCA" -Verbose
    }
    if ($SQAHNum.Number -notcontains $number) {
        Connect-Five9AdminWebService -Credential $SQAHCreds
        Add-Five9DNCNumber $number "SQAH" -Verbose
    }
    if ($HealthNum.Number -notcontains $number) {
        Connect-Five9AdminWebService -Credential $HealthCreds
        Add-Five9DNCNumber $number "Health" -Verbose
    }
    if ($LifeNum.Number -notcontains $number) {
        Connect-Five9AdminWebService -Credential $LifeCreds
        Add-Five9DNCNumber $number "Life" -Verbose
    }
}

#Remove number from all domains if it is matched
function Remove-DNCAllDomains ($number, $matched) {
    $SeniorNum = ($matched | Where-Object {$_.Domain -eq "Senior"})
    $UHCNum = ($matched| Where-Object {$_.Domain -eq "UHC"})
    $CCANum = ($matched | Where-Object {$_.Domain -eq "CCA"})
    $SQAHNum = ($matched | Where-Object {$_.Domain -eq "SQAH"})
    $HealthNum = ($matched | Where-Object {$_.Domain -eq "Health"})
    $LifeNum = ($matched | Where-Object {$_.Domain -eq "Life"})
    if ($SeniorNum.Number -contains $number) {
        Connect-Five9AdminWebService -Credential $SeniorCreds
        Remove-Five9DNCNumber $number -Verbose
    }
    if ($CCANum.Number -contains $number) {
        Connect-Five9AdminWebService -Credential $CCACreds
        Remove-Five9DNCNumber $number -Verbose
    }
    if ($UHCNum.Number -contains $number) {
        Connect-Five9AdminWebService -Credential $SQAHCreds
        Remove-Five9DNCNumber $number -Verbose
    }
    if ($SQAHNum.Number -contains $number) {
        Connect-Five9AdminWebService -Credential $SQAHCreds
        Remove-Five9DNCNumber $number -Verbose
    }
    if ($HealthNum.Number -contains $number) {
        Connect-Five9AdminWebService -Credential $HealthCreds
        Remove-Five9DNCNumber $number -Verbose
    }
    if ($LifeNum.Number -contains $number) {
        Connect-Five9AdminWebService -Credential $LifeCreds
        Remove-Five9DNCNumber $number -Verbose
    }
}