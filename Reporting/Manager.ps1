#!/usr/bin/env powershell

function Show-DomainMatches ($numbersList) {
    $SeniorList = $numbersList | Where-Object {$_.Domain -eq "Senior"}
    $UHCList = $numbersList | Where-Object {$_.Domain -eq "UHC"}
    $CCAList = $numbersList | Where-Object {$_.Domain -eq "CCA"}
    $SQAHList = $numbersList | Where-Object {$_.Domain -eq "SQAH"}
    $HealthList = $numbersList | Where-Object {$_.Domain -eq "Health"}
    $LifeList = $numbersList | Where-Object {$_.Domain -eq "Life"}
    if ($SeniorList) {
        Write-Host "The following numbers were matched in Senior:" -ForegroundColor Yellow
        Write-Host ($SeniorList.Number | Out-String)
    }
    if ($numbersList.Domain -eq "UHC") {
        Write-Host "The following numbers were matched in UHC:" -ForegroundColor Yellow
        Write-Host ($UHCList.Number | Out-String)
    }
    if ($numbersList.Domain -eq "CCA") {
        Write-Host "The following numbers were matched in CCA:" -ForegroundColor Yellow
        Write-Host ($CCAList.Number | Out-String)
    }
    if ($numbersList.Domain -eq "SQAH") {
        Write-Host "The following numbers were matched in SQAH:" -ForegroundColor Yellow
        Write-Host ($SQAHList.Number | Out-String)
    }
    if ($numbersList.Domain -eq "Health") {  
        Write-Host "The following numbers were matched in Health:" -ForegroundColor Yellow
        Write-Host ($HealthList.Number | Out-String)
    }
    if ($numbersList.Domain -eq "Life") {
        Write-Host "The following numbers were matched in Life:" -ForegroundColor Yellow
        Write-Host ($LifeList.Number | Out-String)
    }
    else {
        Write-Host "No numbers were matched in any domain."
    }
}

#This will delete log files older than 30 days
function Start-LogCleanup ($RootDirectory) {
    Get-ChildItem -Path "$RootDirectory\Logging" -Recurse | Where-Object {($_.LastWriteTime -lt (Get-Date).AddDays(-30))} | Remove-Item
}

function Write-Color() {
    Param (
        [string] $text = $(Write-Error "You must specify some text"),
        [switch] $NoNewLine = $false
    )

    $startColor = $host.UI.RawUI.ForegroundColor;

    $text.Split( [char]"{", [char]"}" ) | ForEach-Object { $i = 0; } {
        if ($i % 2 -eq 0) {
            Write-Host $_ -NoNewline;
        } else {
            if ($_ -in [enum]::GetNames("ConsoleColor")) {
                $host.UI.RawUI.ForegroundColor = ($_ -as [System.ConsoleColor]);
            }
        }

        $i++;
    }

    if (!$NoNewLine) {
        Write-Host;
    }
    $host.UI.RawUI.ForegroundColor = $startColor;
}