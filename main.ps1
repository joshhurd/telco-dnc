#!/usr/bin/env powershell

#=====================================================================
# Automated DNC Telecom Applet
# Written By: Joshua Hurd
# Contact: joshua.hurd@selectquote.com, alan.noble@selectquote.com
# Division: Telecom
# Purpose: Manage DNC removals and additions via Google Form
#=====================================================================

#Pull variables from config.txt
Get-Content "$PSScriptRoot\config.txt" | ForEach-Object {
    $var = $_.Split('=')
    New-Variable -Name $var[0] -Value $var[1]
}

#Import other files
Import-Module "$PSScriptRoot\Google\OAuth.ps1"
Import-Module "$PSScriptRoot\Google\GSheetAPI.ps1"
Import-Module "$PSScriptRoot\Google\GMailAPI.ps1"
Import-Module "$PSScriptRoot\Five9\Five9API.ps1"
Import-Module "$PSScriptRoot\Five9\Five9Authorization.ps1"
Import-Module "$PSScriptRoot\Reporting\Manager.ps1"

#Create a logfile in Temp folder
$date = Get-Date -Format "MM-dd-yyyy-HHmm"
Start-Transcript -Path "$PSScriptRoot\Logging\DNCTask-$date.log" -Append

#Connect to Google API and grab sheet data
Write-Host "Grabbing Google Sheet data..." -ForegroundColor Yellow
$GoogleAPICreds = Get-GoogleToken $PSScriptRoot
$SheetData = Get-Sheet $GoogleAPICreds $sheetID $sheetName_Entry

if (!$SheetData) {
    Write-Host "No entries in Google Sheet found, ending task." -ForegroundColor Yellow
    Exit
}

try {
    #Get secure credentials for Five9
    $SeniorCreds = Get-Five9Credentials "Senior" $PSScriptRoot
    $UHCCreds = Get-Five9Credentials "UHC" $PSScriptRoot
    $CCACreds = Get-Five9Credentials "CCA" $PSScriptRoot
    $SQAHCreds = Get-Five9Credentials "SQAH" $PSScriptRoot
    $HealthCreds = Get-Five9Credentials "Health" $PSScriptRoot
    $LifeCreds = Get-Five9Credentials "Life" $PSScriptRoot

    $emptyLines = New-Object System.Collections.ArrayList($null)

    #For each line in data, complete actions
    if ($SheetData){
        #Get a list of numbers that are currently matched in DNC 
        $numberList = New-Object System.Collections.ArrayList($null)
        ForEach ($Line in $SheetData) {
            $number = $Line.'Phone Number to be Changed'
            $Action = $Line.'Add or Remove?'
            $Approved = $Line.Approved
            if (($Action -eq "Add") -or $Approved) {
                $numberList.Add($number) | Out-Null
            }
        }
        $numberUnique = ($numberList | Select-Object -Unique)
        if ($numberUnique) {
            Write-Host "Checking all domains for following numbers:`n" -ForegroundColor Yellow
            Write-Host $numberUnique
            $CheckNumbers = Get-AllFive9DNC $numberUnique $SeniorCreds $UHCCreds $CCACreds $SQAHCreds $HealthCreds $LifeCreds -Verbose
            Show-DomainMatches $checkNumbers
        }

        #Complete actions based on spreadsheet data
        ForEach ($Line in $SheetData) {
            #Define data
            $Timestamp = $Line.Timestamp
            $Action = $Line.'Add or Remove?'
            $Number = $Line.'Phone Number to be Changed'
            $Email = $Line.'Email Address'
            $Approved = $Line.Approved
            $Denied = $Line.Denied
            $Reason = $Line.'Denial Reason'
            $Status = $Line.'API Response'
            $RequestReason = $Line.'Reason for Removal or Addition'
            $Account = $Line.'SelectCare Account Number'
            $Domain = $Line.Division
            $data = New-Object System.Collections.ArrayList($null)
            if ($SheetData.Count -gt 1){
                $index = $SheetData.IndexOf($Line) + 2
            }
            else{
                $index = 2
            }
            
            Write-Host "`n$Line"
            #I had to make all of these repetitive steps entirely separate or it would result in weird bugs ¯\_(ツ)_/¯
            #This removes the API Response code from lines with no data (mostly for edge cases)
            if (!$Timestamp -and !$Action -and !$Number -and !$Email -and !$Approved -and !$Denied -and !$Reason){
                if (!$Status) {
                    Write-Host "Line $index is blank, skipping." -ForegroundColor Red
                }
                else {
                    $data.add( @("", "", "", "", "", "", "", "", "", "", "") ) | Out-Null
                    Write-Host "Line $index is blank, clearing status code." -ForegroundColor Red
                    ForEach-Object {$data} | Write-Host
                    Write-Sheet $GoogleAPICreds $sheetID $sheetName_Entry $data $index
                }
                $emptyLines.add($index)
                continue
            }
            #Return error if missing required fields
            if (!$Timestamp -or !$Action -or !$Number -or !$Email){
                if ($Status -eq "[ERROR] MISSING FIELDS") {
                    Write-Host "Line $index is missing required fields, unable to complete action." -ForegroundColor Red
                }
                else {
                    $data.add( @($Timestamp, $Action, $Number, $Email, $RequestReason, $Account, $Domain, $Approved, $Denied, $Reason, "[ERROR] MISSING FIELDS") ) | Out-Null
                    Write-Host "Line $index is missing required fields, returning error code." -ForegroundColor Red
                    ForEach-Object {$data} | Write-Host
                    Write-Sheet $GoogleAPICreds $sheetID $sheetName_Entry $data $index
                }
                continue
            }
            #Return error if missing denial reason
            if (($Action -eq 'Remove') -and ($Denied -eq "TRUE") -and !$Reason){
                if ($Status -eq "[ERROR] DENIAL REASON REQUIRED"){
                    Write-Host "Line $index is missing denial reason." -ForegroundColor Red
                }
                else {
                    $data.add( @($Timestamp, $Action, $Number, $Email, $RequestReason, $Account, $Domain, $Approved, $Denied, $Reason, "[ERROR] DENIAL REASON REQUIRED") ) | Out-Null
                    Write-Host "Line $index is missing denial reason, returning error code." -ForegroundColor Red
                    ForEach-Object {$data} | Write-Host
                    Write-Sheet $GoogleAPICreds $sheetID $sheetName_Entry $data $index
                }
                continue
            }
            #Return notice if missing approval
            if (($Action -eq "Remove") -and !$Denied -and !$Approved -and ($require_approval_for_remove -ne "FALSE")) {
                if ($Status -eq "[NOTICE] Waiting for approval or denial") {
                    Write-Host "Line $index is still waiting for approval..." -ForegroundColor Yellow
                    #Regex matching because Google abbreviates its time formatting
                    if ($Timestamp -match '[0-9]\/[0-9]\/[0-9]{4} [0-9]{2}\:[0-9]{2}\:[0-9]{2}') {
                        $PostedTime = [datetime]::parseexact($Timestamp, 'M/d/yyyy HH:mm:ss', $null)
                    }
                    elseif ($Timestamp -match '[0-9]\/[0-9]{2}\/[0-9]{4} [0-9]{2}\:[0-9]{2}\:[0-9]{2}') {
                        $PostedTime = [datetime]::parseexact($Timestamp, 'M/dd/yyyy HH:mm:ss', $null)
                    }
                    elseif ($Timestamp -match '[0-9]{2}\/[0-9]\/[0-9]{4} [0-9]{2}\:[0-9]{2}\:[0-9]{2}') {
                        $PostedTime = [datetime]::parseexact($Timestamp, 'MM/d/yyyy HH:mm:ss', $null)
                    }
                    elseif ($Timestamp -match '[0-9]{2}\/[0-9]{2}\/[0-9]{4} [0-9]{2}\:[0-9]{2}\:[0-9]{2}') {
                        $PostedTime = [datetime]::parseexact($Timestamp, 'MM/dd/yyyy HH:mm:ss', $null)
                    }
                    elseif ($Timestamp -match '[0-9]\/[0-9]\/[0-9]{4} [0-9]\:[0-9]{2}\:[0-9]{2}') {
                        $PostedTime = [datetime]::parseexact($Timestamp, 'M/d/yyyy H:mm:ss', $null)
                    }
                    elseif ($Timestamp -match '[0-9]\/[0-9]{2}\/[0-9]{4} [0-9]\:[0-9]{2}\:[0-9]{2}') {
                        $PostedTime = [datetime]::parseexact($Timestamp, 'M/dd/yyyy H:mm:ss', $null)
                    }
                    elseif ($Timestamp -match '[0-9]{2}\/[0-9]\/[0-9]{4} [0-9]\:[0-9]{2}\:[0-9]{2}') {
                        $PostedTime = [datetime]::parseexact($Timestamp, 'MM/d/yyyy H:mm:ss', $null)
                    }
                    elseif ($Timestamp -match '[0-9]{2}\/[0-9]{2}\/[0-9]{4} [0-9]\:[0-9]{2}\:[0-9]{2}') {
                        $PostedTime = [datetime]::parseexact($Timestamp, 'MM/dd/yyyy H:mm:ss', $null)
                    }
                    $CurrentTime = Get-Date
                    $PassedTime = ($CurrentTime - $PostedTime)
                    $CheckLock = Test-Path -Path "$PSScriptRoot\NotifierCooldown.txt"
                    #This adds a 1 day cooldown to the following email so it doesn't spam
                    if ($CheckLock -and ((Get-Item "$PSScriptRoot\NotifierCooldown.txt").LastWriteTime -lt $CurrentTime.AddDays(-1).date)) {
                        Remove-Item -Path "$PSScriptRoot\NotifierCooldown.txt"
                        Start-Sleep -Milliseconds 500 #This fixes a bug where the next iteration happens before the file is removed
                    }
                    $CheckLockVerify = Test-Path -Path "$PSScriptRoot\NotifierCooldown.txt"
                    #Send email notice to "email_notifier_for_pending" if pending is greater than 24 hours
                    if (($PassedTime.Days -gt 0) -and ($CheckLockVerify -eq $False)) {
                        Send-PendingNotice $GoogleAPICreds $email_notifier_for_pending $sheetID
                        "Pending email reminder is on cooldown." | Out-File -FilePath "$PSScriptRoot\NotifierCooldown.txt"
                        Start-Sleep -Milliseconds 500 #This fixes a bug where the next iteration happens before the file is created
                    }
                    continue
                }
                else {
                    Send-ReceiveNotice $GoogleAPICreds $Email $Number
                    $data.add( @($Timestamp, $Action, $Number, $Email, $RequestReason, $Account, $Domain, $Approved, $Denied, $Reason, "[NOTICE] Waiting for approval or denial") ) | Out-Null
                    Write-Host "Line $index is not approved." -ForegroundColor Yellow
                    ForEach-Object {$data} | Write-Host
                    Write-Sheet $GoogleAPICreds $sheetID $sheetName_Entry $data $index
                    continue
                }
            }

            #Make sure number is 10 digits
            if ($Number.length -ne 10) {
                $data.add( @($Timestamp, $Action, $Number, $Email, $RequestReason, $Account, $Domain, $Approved, $Denied, $Reason, "[ERROR] Number is not 10 digits.") ) | Out-Null
                Write-Host "Line $index is not 10 digits, ignoring." -ForegroundColor Yellow
                Write-Sheet $GoogleAPICreds $sheetID $sheetName_Entry $data $index
                continue
            }

            #Check if number is already in every domain, and if not, add it
            if ($Action -eq 'Add') {
                $SN = ($CheckNumbers | Where-Object {$_.Domain -eq "Senior"})
                $UN = ($CheckNumbers| Where-Object {$_.Domain -eq "UHC"})
                $CN = ($CheckNumbers | Where-Object {$_.Domain -eq "CCA"})
                $QN = ($CheckNumbers | Where-Object {$_.Domain -eq "SQAH"})
                $HN = ($CheckNumbers | Where-Object {$_.Domain -eq "Health"})
                $LN = ($CheckNumbers | Where-Object {$_.Domain -eq "Life"})
                if (($SN.Number -and $UN.Number -and $CN.Number -and $QN.Number -and $HN.Number -and $LN.Number) -eq $Number) {
                    Write-Host "Requested number was already in every domain, so no action was taken." -ForegroundColor Yellow
                }
                else {
                    try{
                        Write-Color "{yellow}Adding {white}$Number {yellow}to DNC in all domains..."
                        Add-DNCAllDomains $Number $CheckNumbers
                    }
                    catch {
                        $_
                        $Log = "[ERROR] ADD ERROR, PLEASE CREATE A TICKET"
                        $ErrorReason = "Something went wrong adding $Number on $index."
                        $problem = $true
                    }
                }
            }

            #Remove number from all domains if approved
            if (($Action -eq 'Remove') -and (($Approved -eq "TRUE") -or ($require_approval_for_remove -eq "FALSE"))) {
                if (!$CheckNumbers) {
                    Write-Host "Requested number was not in any domain, so no action was taken." -ForegroundColor Yellow
                }
                if ($CheckNumbers) {
                    try {
                        Write-Color "{yellow}Removing {white}$Number {yellow}from DNC in all domains..."
                        Remove-DNCAllDomains $Number $CheckNumbers
                    }
                    catch {
                        $_
                        $Log = "[ERROR] REMOVE ERROR, PLEASE CREATE A TICKET"
                        $ErrorReason = "Something went wrong removing $Number from $index."
                        $problem = $true
                    }
                }
            }

            #Send denial notice and move line to completed tab if denied
            if (($Action -eq 'Remove') -and ($Denied -eq "TRUE") -and $Reason) {
                Write-Host "Removal denied. Notifying requester and closing request..." -ForegroundColor Yellow
                $data.add( @($Timestamp, $Action, $Number, $Email, $RequestReason, $Account, $Domain, $Approved, $Denied, $Reason, "[INFO] Completed successfully") ) | Out-Null
                Write-SheetAppend $GoogleAPICreds $sheetID $sheetName_Completed $data
                $emptyLines.add($index) #This will add the row to a list to delete all completed rows in bulk
                Send-DenialNotice $GoogleAPICreds $Email $Number $Reason
                Continue
            }

            #If no errors, move data to Completed tab in Google Sheet
            if ($error -or $problem) {
                $data.add( @($Timestamp, $Action, $Number, $Email, $RequestReason, $Account, $Domain, $Approved, $Denied, $Reason, $Log) ) | Out-Null
                Write-Host $ErrorReason -ForegroundColor Red
                ForEach-Object {$data} | Write-Host
                Write-Sheet $GoogleAPICreds $sheetID $sheetName_Entry $data $index
            }
            if (!$error -and !$problem){
                $data.add( @($Timestamp, $Action, $Number, $Email, $RequestReason, $Account, $Domain, $Approved, $Denied, $Reason, "[INFO] Completed successfully") ) | Out-Null
                Write-Host "Copying line to Sheet 2..."
                ForEach-Object {$data} | Write-Host
                Write-SheetAppend $GoogleAPICreds $sheetID $sheetName_Completed $data
                $emptyLines.add($index) #This will add the row to a list to delete all completed rows in bulk
                if ($Action -eq 'Add') {
                    Send-AddedNotice $GoogleAPICreds $Email $Number
                }
                if (($Action -eq 'Remove') -and (($Approved -eq "TRUE") -or ($require_approval_for_remove -eq "FALSE"))) {
                    Send-RemovedNotice $GoogleAPICreds $Email $Number
                }
            }
        }
        #Clean up completed lines
        if ($emptyLines) {
            Write-Host "`nDeleting lines..." -ForegroundColor Yellow
            #If more than one line to be deleted
            if ($emptyLines -gt 1){
                Set-DeleteLine `
                    -token $GoogleAPICreds `
                    -indexes $emptyLines `
                    -sheetName $sheetName_Entry `
                    -sheetID $sheetID
            }
            else {
                Set-DeleteLine `
                    -token $GoogleAPICreds `
                    -index $emptyLines `
                    -sheetName $sheetName_Entry `
                    -sheetID $sheetID
            }
        }
    }
}
catch{
    $_
    Write-Host "Attempting to send error notice to $email_notifier_for_errors..."
    Send-ErrorNotice $GoogleAPICreds $email_notifier_for_errors $_
}
#Remove logs older than 30 days
Write-Host "Cleaning up log files older than 30 days..."
Start-LogCleanup $PSScriptRoot
#Stop logging
Stop-Transcript