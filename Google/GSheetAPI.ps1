#!/usr/bin/env powershell

#Get sheet data based (This will pull all filled out fields and return an array of custom objects)
function Get-Sheet ($token, $sheetID, $sheetName) {
    $uri = "https://sheets.googleapis.com/v4/spreadsheets/$sheetID/values/$sheetName"+"?valueRenderOption=FORMATTED_VALUE"
    try {
        $ProgressPreference = 'SilentlyContinue'
        $result = Invoke-RestMethod -Method GET -Uri $uri -Headers @{"Authorization"="Bearer $token"} -UseBasicParsing
    }
    catch {
        Write-Host "There was an error..." -ForegroundColor Red
        Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
        Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
    }

    # Formatting the returned data
    $sheet = $result.values
    $Rows = $sheet.Count
    $Columns = $sheet[0].Count
    $HeaderRow = 0
    $Header = $sheet[0]
    if ($Rows -lt 2) {
        return $null #Return nothing if only header row
    }
    else {
        foreach ($Row in (($HeaderRow + 1)..($Rows-1))) { 
            $h = [Ordered]@{}
            foreach ($Column in 0..($Columns-1)) {
                if ($sheet[0][$Column].Length -gt 0) {
                    $Name = $Header[$Column]
                    $h.$Name = $Sheet[$Row][$Column]
                }
            }
            [PSCustomObject]$h
        }
    }
}

#Write data to line with specified index
function Write-Sheet ($token, $sheetID, $sheetName, $output, $index){
    $range = "A"+$index+":K"+$index
    $uri = "https://sheets.googleapis.com/v4/spreadsheets/$sheetID/values/$sheetName!$range"+"?valueInputOption=USER_ENTERED"
    $json = @{values=$output} | ConvertTo-Json
    try {
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Method PUT -Uri $uri -Body $json -ContentType "application/json" -Headers @{"Authorization"="Bearer $token"} -UseBasicParsing
        Start-Sleep -Milliseconds 250 #Had to throttle connections beacuse it was going too fast
    }
    catch{
        Write-Host "There was an error..." -ForegroundColor Red
        Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
        Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
    }
}

#Copy sheet data to second tab
function Write-SheetAppend ($token, $sheetID, $sheetName, $output) {
    $range = "A2:A"
    $uri = "https://sheets.googleapis.com/v4/spreadsheets/$sheetID/values/$sheetName!$range"+":append?valueInputOption=RAW&insertDataOption=INSERT_ROWS"
    $json = @{values=$output} | ConvertTo-Json
    try {
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Method POST -Uri $uri -Body $json -ContentType "application/json" -Headers @{"Authorization"="Bearer $token"} -UseBasicParsing
        Start-Sleep -Milliseconds 250 #Had to throttle connections beacuse it was going too fast
    }
    catch{
        Write-Host "There was an error..." -ForegroundColor Red
        Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
        Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
    }
}

#Clear the current working line in the original sheet
function Set-ClearLine ($token, $sheetID, $sheetName, $index){
    $range = "A"+$index+":K"+$index
    $uri = "https://sheets.googleapis.com/v4/spreadsheets/$sheetID/values/$sheetName!$range"+":clear"
    try {
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Method POST -Uri $uri -Body "" -ContentType "application/json" -Headers @{"Authorization"="Bearer $token"} -UseBasicParsing
        Start-Sleep -Milliseconds 250 #Had to throttle connections beacuse it was going too fast
    }
    catch{
        Write-Host "There was an error..." -ForegroundColor Red
        Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
        Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
    }
}

#This function returns the unique identifier for a page in a spreadsheet with multiple pages
function Get-SheetToken ($sheetID, $sheetName, $token) {
    $uri = "https://sheets.googleapis.com/v4/spreadsheets/$sheetID"
    $spreadSheetToken = Invoke-RestMethod -Method GET -Uri $uri -Headers @{"Authorization"="Bearer $token"}
    $sheetToken = ($spreadSheetToken.sheets.properties | Where-Object {$_.title -eq $sheetName}).sheetID
    Start-Sleep -Milliseconds 250 #Had to throttle connections beacuse it was going too fast
    return $sheetToken
}

#This function will delete lines at the indexes provided
function Set-DeleteLine {
    Param (
        [Parameter(Mandatory)]
        [string]$token,

        [Parameter(Mandatory,ParameterSetName='Single')]
        [int]$index,

        [Parameter(Mandatory,ParameterSetName='Multiple')]
        [array]$indexes,

        [Parameter(Mandatory)]
        [string]$sheetName,

        [Parameter(Mandatory)]
        [string]$sheetID
    )
    
    $sheetToken = Get-SheetToken $sheetID $sheetName $token

    #Allow for bulk deletion by adding multiples to a list
    $requestArray = New-Object System.Collections.ArrayList($null)
    if ($indexes -gt 1) {
        ForEach ($index in $indexes) {
            $iteration = $indexes.IndexOf($index)
            $startIndex = $index - $iteration - 1
            $endIndex = $startIndex + 1
            [void]$requestArray.add(@{"deleteDimension" = @{"range" = @{"sheetId" = $sheetToken; "dimension" = "ROWS"; `
                "startIndex" = $startIndex; "endIndex" = $endIndex}}})
        }
    }
    else {
        $startIndex = $index - 1
        [void]$requestArray.add(@{"deleteDimension" = @{"range" = @{"sheetId" = $sheetToken; "dimension" = "ROWS"; `
            "startIndex" = $startIndex; "endIndex" = $index}}})
    }
    $uri = "https://sheets.googleapis.com/v4/spreadsheets/$sheetID"+":batchUpdate"
    $json = @{requests=$requestArray} | ConvertTo-Json -Depth 20
    try{
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Method POST -Uri $uri -Body $json -ContentType "application/json" -Headers @{"Authorization"="Bearer $token"}
        Start-Sleep -Milliseconds 250 #Had to throttle connections beacuse it was going too fast
    }
    catch{
        Write-Host "There was an error..." -ForegroundColor Red
        Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
        Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
    }
}

#This function will add 100 blank rows at the bottom of the sheet
function Set-AddLines ($token, $sheetID, $sheetName) {
    $sheetToken = Get-SheetToken $sheetID $sheetName $token
    $uri = "https://sheets.googleapis.com/v4/spreadsheets/$sheetID"+":bachUpdate"
    $json = @{requests = @{"appendDimension" = @{"sheetId" = $sheetToken; "dimension" = "ROWS"; "length" = 100}}} | ConvertTo-Json -Depth 20
    try {
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Method POST -Uri $uri -Body $json -ContentType "application/json" -Headers @{"Authorization"="Bearer $token"} -UseBasicParsing
        Start-Sleep -Milliseconds 250 #Had to throttle connections beacuse it was going too fast
    }
    catch{
        Write-Host "There was an error..." -ForegroundColor Red
        Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
        Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
    }
}

#####THESE FUNCTIONS ARE FOR SERVICE ACCOUNT AUTHENTICATION AND DEPCRECATED, FOR REFERENCE ONLY#####
#Functions imported from UNM-Google and modified to fit this project: https://github.com/umn-microsoft-automation/UMN-Google
# function Get-GoogleCreds ($certPath, $iss) {
#     # Set security protocol to TLS 1.2 to avoid TLS errors
#     [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

#     # Google API Authozation
#     $scope = "https://www.googleapis.com/auth/spreadsheets https://www.googleapis.com/auth/gmail.modify"

#     #Pulls the cert pass from an encrypted file. This file needs to be regenerated when moved, encryption is basd on user and system ID.
#     $SecureFile = "$Dir\Secure\secure.txt"
#     $SecureString = ConvertTo-SecureString -String (Get-Content -Path $SecureFile)
#     $Pointer = [Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecureString)
#     $certPswd = [Runtime.InteropServices.Marshal]::PtrToStringAuto($Pointer) 

#     try {
#         $accessToken = Get-GAPIAuthentication $scope $certPath $certPswd $iss
#         return $accessToken
#     } catch {
#         $err = $_.Exception
#         $err | Select-Object -Property *
#         "Response: "
#         $err.Response
#     }
# }

# #This returns a usable access token using security credentials
# function Get-GAPIAuthentication ($scope, $certPath, $certPswd, $iss){
#     # build JWT header
#     $headerJSON = [Ordered]@{
#         alg = "RS256"
#         typ = "JWT"
#     } | ConvertTo-Json -Compress
#     $headerBase64 = ConvertTo-Base64URL -text $headerJSON

#     # Build claims for JWT
#     $now = (Get-Date).ToUniversalTime()
#     $iat = [Math]::Floor([decimal](Get-Date($now) -UFormat "%s"))
#     $exp = [Math]::Floor([decimal](Get-Date($now.AddMinutes(59)) -UFormat "%s")) 
#     $aud = "https://www.googleapis.com/oauth2/v4/token"
#     $claimsJSON = [Ordered]@{
#         iss = $iss
#         scope = $scope
#         aud = $aud
#         exp = $exp
#         iat = $iat
#     } | ConvertTo-Json -Compress

#     $claimsBase64 = ConvertTo-Base64URL -text $claimsJSON

#     ################# Create JWT
#     # Prep JWT certificate signing
#     $googleCert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2($certPath, $certPswd,[System.Security.Cryptography.X509Certificates.X509KeyStorageFlags]::Exportable ) 
#     $rsaPrivate = $googleCert.PrivateKey 
#     $rsa = New-Object System.Security.Cryptography.RSACryptoServiceProvider 
#     $null = $rsa.ImportParameters($rsaPrivate.ExportParameters($true))
    
#     # Signature is our base64urlencoded header and claims, delimited by a period.
#     $toSign = [System.Text.Encoding]::UTF8.GetBytes($headerBase64 + "." + $claimsBase64)
#     $signature = ConvertTo-Base64URL -Bytes $rsa.SignData($toSign,"SHA256") ## this needs to be converted back to regular text
    
#     # Build request
#     $jwt = $headerBase64 + "." + $claimsBase64 + "." + $signature
#     $fields = 'grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer&assertion='+$jwt

#     # Fetch token
#     $response = Invoke-RestMethod -Uri "https://www.googleapis.com/oauth2/v4/token" -Method Post -Body $fields -ContentType "application/x-www-form-urlencoded"
#     return $response.access_token
# }