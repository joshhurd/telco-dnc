#!/usr/bin/env powershell

#This wraps the email in Base64 which is required for Google messaging API.
Function Get-Base64Url([string]$MsgIn) {
    $InputBytes =  [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($MsgIn))
    
    # "Url-Safe" base64 encodeing
    $InputBytes = $InputBytes.Replace('+', '-').Replace('/', '_').Replace("=", "")
    return $InputBytes
}

#Generate an email for receive notice
function Send-ReceiveNotice ($token, $requester, $DNIS) {
    $to = "To: $requester`n"
    $cc = "CC: `n"
    $reply = "Reply-To: telecom_ps@selectquote.com`n"
    $bcc = "Bcc: `n"
    $from = "From: telecom_ps@selectquote.com`n"
    $importance = "Importance: 0`n"
    $subject = "Subject: [Confirmation] DNC Removal Request`n"

    $body = @'
Content-Type: multipart/alternative; boundary="0000000000002efc5d05dc1b97b3"

--0000000000002efc5d05dc1b97b3
Content-Type: text/plain; charset="UTF-8"

Hello,

This is an automated notice to let you know that your DNC removal request
for 
'@ + $DNIS + 
@'
 has been received and is being reviewed.
Once your request has been reviewed, you will receive a notification if it
has been completed, or the reasons for why it was declined.


*###This is an automated message and this inbox is not monitored###*
*------------------------------------------------------------------*
*If you need assistance relating to Telecom services please
 email telcoteam@selectquote.com <telcoteam@selectquote.com>.*
*If you wish to appeal a DNC denial, please email donotcall@selectquote.com
<donotcall@selectquote.com>*
*------------------------------------------------------------------*

*Telecom| SelectQuote IT*

--0000000000002efc5d05dc1b97b3
Content-Type: text/html; charset="UTF-8"
Content-Transfer-Encoding: quoted-printable

<div dir=3D"ltr">Hello,<div><br></div><div>This is an automated notice to l=
et you know that your DNC removal request for 
'@ + $DNIS + 
@'
 has been received =
and is being reviewed.=C2=A0</div><div>Once your request has been reviewed,=
 you will receive a notification if it has been completed, or the reasons f=
or why it was declined.</div><div><br></div><div><br></div><div><i><font fa=
ce=3D"monospace">###This is an automated message and this inbox is not moni=
tored###</font></i></div><div><i><font face=3D"monospace">-----------------=
-------------------------------------------------</font></i></div><div><i><=
font face=3D"monospace">If you need assistance relating to Telecom services=
 please email=C2=A0<b><a href=3D"mailto:telcoteam@selectquote.com" target=
=3D"_blank">telcoteam@selectquote.com</a></b>.</font></i></div><div><i><fon=
t face=3D"monospace">If you wish to appeal a DNC denial, please email=C2=A0=
<b><a href=3D"mailto:donotcall@selectquote.com" target=3D"_blank">donotcall=
@selectquote.com</a></b></font></i></div><div><i><font face=3D"monospace">-=
-----------------------------------------------------------------</font></i=
><br></div><div><br></div><div><div><div dir=3D"ltr"><div dir=3D"ltr"><div>=
<font color=3D"#e69138" face=3D"trebuchet ms, sans-serif"><b>Telecom|=C2=A0=
SelectQuote IT</b></font></div><div><font color=3D"#e69138" face=3D"trebuch=
et ms, sans-serif"><b><img src=3D"https://ci6.googleusercontent.com/proxy/0=
s5A5S1oxPltkTT5Uw_nslUH_T4IPOytTwMyCpy6ovJdkPSW6thb6VGzFc0CsT5uPb9Blm9KrKrG=
3D926NZr7b-p1pnk0kwZmakLcdyAbtN2CrIaJg_tGk2OuSkUE5hZ62KM7sujmRB_Tw2pJ7-8eJE=
7bztFEit9JmkUG9lIdrThD3YTEbAxSin8JX25agQu5OZ-AL4YcPOT5x2raA=3Ds0-d-e1-ft#ht=
tps://docs.google.com/uc?export=3Ddownload&amp;id=3D1VRrr4V_Ws9EuxFOM5_t4m6=
HYpCDDxPrd&amp;revid=3D0B2Alan3QKn4tbHU2WVc2eEp3MUdVeFNQZ0xzN3dFc2k3bWY0PQ"=
class=3D"gmail-CToWUd"></b></font></div></div></div></div></div></div>

--0000000000002efc5d05dc1b97b3--
'@

    $MsgSW = ($to + $cc + $reply + $bcc + $from + $importance + $subject + $body)
    $EncodedEmail = Get-Base64Url $MsgSW

    $Content = @{ "raw" = $EncodedEmail; } | ConvertTo-Json
    try {
        Write-Host "Sending receive notice email " -NoNewline -ForegroundColor Yellow
        Write-Host $to -NoNewline
        Write-Host "..." -ForegroundColor Yellow
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Uri "https://www.googleapis.com/gmail/v1/users/me/messages/send?access_token=$token" -Method POST -ErrorAction Stop -Body $Content -ContentType "Application/Json"
    }
    catch{
        Write-Host "There was an error..." -ForegroundColor Red
        Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
        Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
    }
}

function Send-AddedNotice ($token, $requester, $DNIS) {
    $to = "To: $requester`n"
    $cc = "CC: `n"
    $reply = "Reply-To: telecom_ps@selectquote.com`n"
    $bcc = "Bcc: `n"
    $from = "From: telecom_ps@selectquote.com`n"
    $importance = "Importance: 0`n"
    $subject = "Subject: [Completed] DNC Addition Request`n"

    $body = @'
Content-Type: multipart/alternative; boundary="000000000000c0ba9205dc1bae71"

--000000000000c0ba9205dc1bae71
Content-Type: text/plain; charset="UTF-8"

Hello,

This is an automated notice to let you know that your DNC addition request
for 
'@ + $DNIS + 
@'
 has been received and is now completed.


*###This is an automated message and this inbox is not monitored###*
*------------------------------------------------------------------*
*If you need assistance relating to Telecom services please
email telcoteam@selectquote.com <telcoteam@selectquote.com>.*
*If you wish to appeal a DNC denial, please email donotcall@selectquote.com
<donotcall@selectquote.com>*
*------------------------------------------------------------------*

*Telecom| SelectQuote IT*

--000000000000c0ba9205dc1bae71
Content-Type: text/html; charset="UTF-8"
Content-Transfer-Encoding: quoted-printable

<div dir=3D"ltr"><div dir=3D"ltr" class=3D"gmail_signature" data-smartmail=
=3D"gmail_signature"><div dir=3D"ltr"><div style=3D"color:rgb(34,34,34)">He=
llo,<div><br></div><div>This is an automated notice to let you know that yo=
ur DNC addition request for 
'@ + $DNIS + 
@'
 has been received and is now complet=
ed.</div><div><br></div><div><br></div><div><i><font face=3D"monospace">###=
This is an automated message and this inbox is not monitored###</font></i><=
/div><div><i><font face=3D"monospace">-------------------------------------=
-----------------------------</font></i></div><div><i><font face=3D"monospa=
ce">If you need assistance relating to Telecom services please email=C2=A0<=
b><a href=3D"mailto:telcoteam@selectquote.com" target=3D"_blank">telcoteam@=
selectquote.com</a></b>.</font></i></div><div><i><font face=3D"monospace">I=
f you wish to appeal a DNC denial, please email=C2=A0<b><a href=3D"mailto:d=
onotcall@selectquote.com" target=3D"_blank">donotcall@selectquote.com</a></=
b></font></i></div><div><i><font face=3D"monospace">-----------------------=
-------------------------------------------</font></i><br></div><div><br></=
div><div><div dir=3D"ltr"><div dir=3D"ltr"><div><font color=3D"#e69138" fac=
e=3D"trebuchet ms, sans-serif"><b>Telecom|=C2=A0SelectQuote IT</b></font></=
div></div></div></div><b style=3D"color:rgb(230,145,56);font-family:&quot;t=
rebuchet ms&quot;,sans-serif"><img src=3D"https://ci6.googleusercontent.com=
/proxy/0s5A5S1oxPltkTT5Uw_nslUH_T4IPOytTwMyCpy6ovJdkPSW6thb6VGzFc0CsT5uPb9B=
lm9KrKrG3D926NZr7b-p1pnk0kwZmakLcdyAbtN2CrIaJg_tGk2OuSkUE5hZ62KM7sujmRB_Tw2=
pJ7-8eJE7bztFEit9JmkUG9lIdrThD3YTEbAxSin8JX25agQu5OZ-AL4YcPOT5x2raA=3Ds0-d-=
e1-ft#https://docs.google.com/uc?export=3Ddownload&amp;id=3D1VRrr4V_Ws9EuxF=
OM5_t4m6HYpCDDxPrd&amp;revid=3D0B2Alan3QKn4tbHU2WVc2eEp3MUdVeFNQZ0xzN3dFc2k=
3bWY0PQ" class=3D"gmail-CToWUd"></b><font color=3D"#e69138" face=3D"trebuch=
et ms, sans-serif"><b><br></b></font></div></div></div></div>

--000000000000c0ba9205dc1bae71--
'@

    $MsgSW = ($to + $cc + $reply + $bcc + $from + $importance + $subject + $body)
    $EncodedEmail = Get-Base64Url $MsgSW

    $Content = @{ "raw" = $EncodedEmail; } | ConvertTo-Json
    try {
        Write-Host "Sending completed notice email " -NoNewline -ForegroundColor Yellow
        Write-Host $to -NoNewline
        Write-Host "..." -ForegroundColor Yellow
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Uri "https://www.googleapis.com/gmail/v1/users/me/messages/send?access_token=$token" -Method POST -ErrorAction Stop -Body $Content -ContentType "Application/Json"
    }
    catch{
        Write-Host "There was an error..." -ForegroundColor Red
        Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
        Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
    }
}

function Send-RemovedNotice ($token, $requester, $DNIS) {
    $to = "To: $requester`n"
    $cc = "CC: `n"
    $reply = "Reply-To: telecom_ps@selectquote.com`n"
    $bcc = "Bcc: `n"
    $from = "From: telecom_ps@selectquote.com`n"
    $importance = "Importance: 0`n"
    $subject = "Subject: [Completed] DNC Removal Request`n"

    $body = @'
Content-Type: multipart/alternative; boundary="00000000000034f2d405dc1c4e5e"

--00000000000034f2d405dc1c4e5e
Content-Type: text/plain; charset="UTF-8"

Hello,

This is an automated notice to let you know that your DNC removal request
for 
'@ + $DNIS + 
@'
 has been approved and is now completed.


*###This is an automated message and this inbox is not monitored###*
*------------------------------------------------------------------*
*If you need assistance relating to Telecom services please
email telcoteam@selectquote.com <telcoteam@selectquote.com>.*
*If you wish to appeal a DNC denial, please email donotcall@selectquote.com
<donotcall@selectquote.com>*
*------------------------------------------------------------------*

*Telecom| SelectQuote IT*

--00000000000034f2d405dc1c4e5e
Content-Type: text/html; charset="UTF-8"
Content-Transfer-Encoding: quoted-printable

<div dir=3D"ltr">Hello,<div><br></div><div>This is an automated notice to l=
et you know that your DNC removal request for 
'@ + $DNIS + 
@'
 has been approved =
and is now completed.</div><div><br></div><div><br></div><div><i><font face=
=3D"monospace">###This is an automated message and this inbox is not monito=
red###</font></i></div><div><i><font face=3D"monospace">-------------------=
-----------------------------------------------</font></i></div><div><i><fo=
nt face=3D"monospace">If you need assistance relating to Telecom services p=
lease email=C2=A0<b><a href=3D"mailto:telcoteam@selectquote.com" target=3D"=
_blank">telcoteam@selectquote.com</a></b>.</font></i></div><div><i><font fa=
ce=3D"monospace">If you wish to appeal a DNC denial, please email=C2=A0<b><=
a href=3D"mailto:donotcall@selectquote.com" target=3D"_blank">donotcall@sel=
ectquote.com</a></b></font></i></div><div><i><font face=3D"monospace">-----=
-------------------------------------------------------------</font></i><br=
></div><div><br></div><div><div dir=3D"ltr"><div dir=3D"ltr"><div><font col=
or=3D"#e69138" face=3D"trebuchet ms, sans-serif"><b>Telecom|=C2=A0SelectQuo=
te IT</b></font></div></div></div></div><b style=3D"color:rgb(230,145,56);f=
ont-family:&quot;trebuchet ms&quot;,sans-serif"><img src=3D"https://ci6.goo=
gleusercontent.com/proxy/0s5A5S1oxPltkTT5Uw_nslUH_T4IPOytTwMyCpy6ovJdkPSW6t=
hb6VGzFc0CsT5uPb9Blm9KrKrG3D926NZr7b-p1pnk0kwZmakLcdyAbtN2CrIaJg_tGk2OuSkUE=
5hZ62KM7sujmRB_Tw2pJ7-8eJE7bztFEit9JmkUG9lIdrThD3YTEbAxSin8JX25agQu5OZ-AL4Y=
cPOT5x2raA=3Ds0-d-e1-ft#https://docs.google.com/uc?export=3Ddownload&amp;id=
=3D1VRrr4V_Ws9EuxFOM5_t4m6HYpCDDxPrd&amp;revid=3D0B2Alan3QKn4tbHU2WVc2eEp3M=
UdVeFNQZ0xzN3dFc2k3bWY0PQ" class=3D"gmail-CToWUd"></b><br></div>

--00000000000034f2d405dc1c4e5e--
'@

    $MsgSW = ($to + $cc + $reply + $bcc + $from + $importance + $subject + $body)
    $EncodedEmail = Get-Base64Url $MsgSW

    $Content = @{ "raw" = $EncodedEmail; } | ConvertTo-Json
    try {
        Write-Host "Sending completed notice email " -NoNewline -ForegroundColor Yellow
        Write-Host $to -NoNewline
        Write-Host "..." -ForegroundColor Yellow
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Uri "https://www.googleapis.com/gmail/v1/users/me/messages/send?access_token=$token" -Method POST -ErrorAction Stop -Body $Content -ContentType "Application/Json"
    }
    catch{
        Write-Host "There was an error..." -ForegroundColor Red
        Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
        Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
    }
}

function Send-DenialNotice ($token, $requester, $DNIS, $DenialReason) {
    $to = "To: $requester`n"
    $cc = "CC: `n"
    $reply = "Reply-To: telecom_ps@selectquote.com`n"
    $bcc = "Bcc: `n"
    $from = "From: telecom_ps@selectquote.com`n"
    $importance = "Importance: 0`n"
    $subject = "Subject: [Notice] DNC Removal Request Denied`n"

    $body = @'
Content-Type: multipart/alternative; boundary="000000000000da5a9a05dc1c7732"

--000000000000da5a9a05dc1c7732
Content-Type: text/plain; charset="UTF-8"

Hello,

This is an automated notice to let you know that your DNC removal request
for 
'@ + $DNIS + 
@'
 has been denied for the following provided reason:

*
'@ + $DenialReason + 
@'
*

This number will stay on the DNC list and further contact is prohibited.


*###This is an automated message and this inbox is not monitored###*
*------------------------------------------------------------------*
*If you need assistance relating to Telecom services please
email telcoteam@selectquote.com <telcoteam@selectquote.com>.*
*If you wish to appeal a DNC denial, please email donotcall@selectquote.com
<donotcall@selectquote.com>*
*------------------------------------------------------------------*

*Telecom| SelectQuote IT*

--000000000000da5a9a05dc1c7732
Content-Type: text/html; charset="UTF-8"
Content-Transfer-Encoding: quoted-printable

<div dir=3D"ltr">Hello,<div><br></div><div>This is an automated notice to l=
et you know that your DNC removal request for 
'@ + $DNIS + 
@'
 has been denied fo=
r the following provided reason:</div><div><br></div><div><b>
'@ + $DenialReason + 
@'
</b></div><div><br></div><div>This number will s=
tay on the DNC list and further contact is prohibited.</div><div><br></div>=
<div><br></div><div><i><font face=3D"monospace">###This is an automated mes=
sage and this inbox is not monitored###</font></i></div><div><i><font face=
=3D"monospace">------------------------------------------------------------=
------</font></i></div><div><i><font face=3D"monospace">If you need assista=
nce relating to Telecom services please email=C2=A0<b><a href=3D"mailto:tel=
coteam@selectquote.com" target=3D"_blank">telcoteam@selectquote.com</a></b>=
.</font></i></div><div><i><font face=3D"monospace">If you wish to appeal a =
DNC denial, please email=C2=A0<b><a href=3D"mailto:donotcall@selectquote.co=
m" target=3D"_blank">donotcall@selectquote.com</a></b></font></i></div><div=
><i><font face=3D"monospace">----------------------------------------------=
--------------------</font></i><br></div><div><br></div><div><div dir=3D"lt=
r"><div dir=3D"ltr"><div><font color=3D"#e69138" face=3D"trebuchet ms, sans=
-serif"><b>Telecom|=C2=A0SelectQuote IT</b></font></div></div></div></div><=
b style=3D"color:rgb(230,145,56);font-family:&quot;trebuchet ms&quot;,sans-=
serif"><img src=3D"https://ci6.googleusercontent.com/proxy/0s5A5S1oxPltkTT5=
Uw_nslUH_T4IPOytTwMyCpy6ovJdkPSW6thb6VGzFc0CsT5uPb9Blm9KrKrG3D926NZr7b-p1pn=
k0kwZmakLcdyAbtN2CrIaJg_tGk2OuSkUE5hZ62KM7sujmRB_Tw2pJ7-8eJE7bztFEit9JmkUG9=
lIdrThD3YTEbAxSin8JX25agQu5OZ-AL4YcPOT5x2raA=3Ds0-d-e1-ft#https://docs.goog=
le.com/uc?export=3Ddownload&amp;id=3D1VRrr4V_Ws9EuxFOM5_t4m6HYpCDDxPrd&amp;=
revid=3D0B2Alan3QKn4tbHU2WVc2eEp3MUdVeFNQZ0xzN3dFc2k3bWY0PQ" class=3D"gmail=
-CToWUd"></b><br></div>

--000000000000da5a9a05dc1c7732--
'@

    $MsgSW = ($to + $cc + $reply + $bcc + $from + $importance + $subject + $body)
    $EncodedEmail = Get-Base64Url $MsgSW

    $Content = @{ "raw" = $EncodedEmail; } | ConvertTo-Json
    try {
        Write-Host "Sending completed notice email " -NoNewline -ForegroundColor Yellow
        Write-Host $to -NoNewline
        Write-Host "..." -ForegroundColor Yellow
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Uri "https://www.googleapis.com/gmail/v1/users/me/messages/send?access_token=$token" -Method POST -ErrorAction Stop -Body $Content -ContentType "Application/Json"
    }
    catch{
        Write-Host "There was an error..." -ForegroundColor Red
        Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
        Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
    }
}

function Send-ErrorNotice ($token, $emailDestination, $errorContent) {
    $to = "To: $emailDestination`n"
    $cc = "CC: `n"
    $reply = "Reply-To: telecom_ps@selectquote.com`n"
    $bcc = "Bcc: `n"
    $from = "From: telecom_ps@selectquote.com`n"
    $importance = "Importance: 0`n"
    $subject = "Subject: [ERROR NOTICE] DNC Manager Failure`n"
    $body = @'
Content-Type: multipart/alternative; boundary="000000000000d921e005de39ea79"

--000000000000d921e005de39ea79
Content-Type: text/plain; charset="UTF-8"

The DNC Manager encountered a fatal error:

'@ + $errorContent + 
@'

*###This is an automated message and this inbox is not monitored###*
*------------------------------------------------------------------*
See https://bitbucket.org/joshhurd/telco-dnc/src/main/ for more details on
this project.
*------------------------------------------------------------------*

*Telecom| SelectQuote IT*

--000000000000d921e005de39ea79
Content-Type: text/html; charset="UTF-8"
Content-Transfer-Encoding: quoted-printable

<div dir=3D"ltr">The DNC Manager encountered a fatal error:<div><br></div><=
div>
'@ + $errorContent + 
@'
<br><div><br></div><div><i><font face=3D"monospace">###Thi=
s is an automated message and this inbox is not monitored###</font></i></di=
v><div><i><font face=3D"monospace">----------------------------------------=
--------------------------</font></i></div><div>See=C2=A0<a href=3D"https:/=
/bitbucket.org/joshhurd/telco-dnc/src/main/">https://bitbucket.org/joshhurd=
/telco-dnc/src/main/</a> for more details on this project.</div><div><i><fo=
nt face=3D"monospace">-----------------------------------------------------=
-------------</font></i><br></div><div><br></div><div><div dir=3D"ltr"><div=
    dir=3D"ltr"><div><font color=3D"#e69138" face=3D"trebuchet ms, sans-serif"=
><b>Telecom|=C2=A0SelectQuote IT</b></font></div></div></div></div><b style=
=3D"color:rgb(230,145,56);font-family:&quot;trebuchet ms&quot;,sans-serif">=
<img src=3D"https://ci6.googleusercontent.com/proxy/0s5A5S1oxPltkTT5Uw_nslU=
H_T4IPOytTwMyCpy6ovJdkPSW6thb6VGzFc0CsT5uPb9Blm9KrKrG3D926NZr7b-p1pnk0kwZma=
kLcdyAbtN2CrIaJg_tGk2OuSkUE5hZ62KM7sujmRB_Tw2pJ7-8eJE7bztFEit9JmkUG9lIdrThD=
3YTEbAxSin8JX25agQu5OZ-AL4YcPOT5x2raA=3Ds0-d-e1-ft#https://docs.google.com/=
uc?export=3Ddownload&amp;id=3D1VRrr4V_Ws9EuxFOM5_t4m6HYpCDDxPrd&amp;revid=
=3D0B2Alan3QKn4tbHU2WVc2eEp3MUdVeFNQZ0xzN3dFc2k3bWY0PQ" class=3D"gmail-CToW=
Ud"></b></div></div>

--000000000000d921e005de39ea79--
'@

    $MsgSW = ($to + $cc + $reply + $bcc + $from + $importance + $subject + $body)
    $EncodedEmail = Get-Base64Url $MsgSW

    $Content = @{ "raw" = $EncodedEmail; } | ConvertTo-Json
    try {
        Write-Host "Sending error email " -NoNewline -ForegroundColor Yellow
        Write-Host $to -NoNewline
        Write-Host "..." -ForegroundColor Yellow
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Uri "https://www.googleapis.com/gmail/v1/users/me/messages/send?access_token=$token" -Method POST -ErrorAction Stop -Body $Content -ContentType "Application/Json"
    }
    catch{
        Write-Host "There was an error..." -ForegroundColor Red
        Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
        Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
    }
}

function Send-PendingNotice ($token, $emailDestination, $SheetID) {
    $to = "To: $emailDestination`n"
    $cc = "CC: `n"
    $reply = "Reply-To: telecom_ps@selectquote.com`n"
    $bcc = "Bcc: `n"
    $from = "From: telecom_ps@selectquote.com`n"
    $importance = "Importance: 0`n"
    $subject = "Subject: [Notice] DNC Pending Notice`n"

    $body = @'
Content-Type: multipart/alternative; boundary="00000000000056cf9205de37efcf"

--00000000000056cf9205de37efcf
Content-Type: text/plain; charset="UTF-8"

Hello,

This is an automated notice to let you know that the Telecom automated DNC
manager has encountered pending requests and is awaiting approval:

https://docs.google.com/spreadsheets/d/
'@+ $SheetID + 
@'

Thank you.

*###This is an automated message and this inbox is not monitored###*
*------------------------------------------------------------------*
*If you need assistance relating to Telecom services please
email telcoteam@selectquote.com <telcoteam@selectquote.com>.*
*This email notice has a cooldown of 24 hours before it will be sent again.*
*------------------------------------------------------------------*

*Telecom| SelectQuote IT*

--00000000000056cf9205de37efcf
Content-Type: text/html; charset="UTF-8"
Content-Transfer-Encoding: quoted-printable

<div dir=3D"ltr">Hello,<div><br></div><div>This is an automated notice to l=
et you know that the Telecom automated DNC manager has encountered pending req=
uests and is awaiting approval:</div><div><br></div><div><a href=3D"https:/=
/docs.google.com/spreadsheets/d/
'@+ $SheetID + 
@'
">https://docs.google.com/spreadsheets/d/
'@+ $SheetID + 
@'
=
<br></div><div><br></div><div>Thank you.</div><div><br></di=
v><div><i><font face=3D"monospace">###This is an automated message and this=
    inbox is not monitored###</font></i></div><div><i><font face=3D"monospace"=
>------------------------------------------------------------------</font><=
/i></div><div><i><font face=3D"monospace">If you need assistance relating t=
o Telecom services please email=C2=A0<b><a href=3D"mailto:telcoteam@selectq=
uote.com" target=3D"_blank">telcoteam@selectquote.com</a></b>.</font></i></=
div><div><font face=3D"monospace"><i>This email notice has a cooldown of 24=
    hours before it will be sent again.</i></font></div><div><i><font face=3D"=
monospace">----------------------------------------------------------------=
--</font></i><br></div><div><br></div><div><div dir=3D"ltr"><div dir=3D"ltr=
"><div><font color=3D"#e69138" face=3D"trebuchet ms, sans-serif"><b>Telecom=
|=C2=A0SelectQuote IT</b></font></div></div></div></div><b style=3D"color:r=
gb(230,145,56);font-family:&quot;trebuchet ms&quot;,sans-serif"><img src=3D=
"https://ci6.googleusercontent.com/proxy/0s5A5S1oxPltkTT5Uw_nslUH_T4IPOytTw=
MyCpy6ovJdkPSW6thb6VGzFc0CsT5uPb9Blm9KrKrG3D926NZr7b-p1pnk0kwZmakLcdyAbtN2C=
rIaJg_tGk2OuSkUE5hZ62KM7sujmRB_Tw2pJ7-8eJE7bztFEit9JmkUG9lIdrThD3YTEbAxSin8=
JX25agQu5OZ-AL4YcPOT5x2raA=3Ds0-d-e1-ft#https://docs.google.com/uc?export=
=3Ddownload&amp;id=3D1VRrr4V_Ws9EuxFOM5_t4m6HYpCDDxPrd&amp;revid=3D0B2Alan3=
QKn4tbHU2WVc2eEp3MUdVeFNQZ0xzN3dFc2k3bWY0PQ" class=3D"gmail-CToWUd"></b><br=
></div>

--00000000000056cf9205de37efcf--
'@

    $MsgSW = ($to + $cc + $reply + $bcc + $from + $importance + $subject + $body)
    $EncodedEmail = Get-Base64Url $MsgSW

    $Content = @{ "raw" = $EncodedEmail; } | ConvertTo-Json
    try {
        Write-Host "Sending pending request email " -NoNewline -ForegroundColor Yellow
        Write-Host $to -NoNewline
        Write-Host "..." -ForegroundColor Yellow
        $ProgressPreference = 'SilentlyContinue'
        Invoke-RestMethod -Uri "https://www.googleapis.com/gmail/v1/users/me/messages/send?access_token=$token" -Method POST -ErrorAction Stop -Body $Content -ContentType "Application/Json"
    }
    catch{
        Write-Host "There was an error..." -ForegroundColor Red
        Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
        Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
    }
}
