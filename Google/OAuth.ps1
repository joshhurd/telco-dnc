#!/usr/bin/env powershell

function Get-RefreshToken ($clientID, $clientSecret, $refreshToken, $RootDirectory) {
    $headers = @{ 
        "Content-Type" = "application/x-www-form-urlencoded" 
    } 
    $body = @{
        client_id     = $clientID
        client_secret = $clientSecret
        refresh_token = $refreshToken
        grant_type    = 'refresh_token'
    }
    $params = @{
        'Uri'         = 'https://accounts.google.com/o/oauth2/token'
        'ContentType' = 'application/x-www-form-urlencoded'
        'Method'      = 'POST'
        'Headers'     = $headers
        'Body'        = $body
    }

    $tokens = Invoke-RestMethod @params

    #Overwrite existing access token
    ConvertTo-SecureString -String $tokens.access_token -AsPlainText -Force | 
        ConvertFrom-SecureString | 
        Out-File "$RootDirectory\Secure\Google_AccessToken.txt" -Encoding UTF8

    return $tokens
}

function Get-GoogleToken ($RootDirectory) {
    if (Test-Path -path "$RootDirectory\Secure\Google_OAuth.txt") {
        $clientID_enc = (Get-Content "$RootDirectory\Secure\Google_OAuth.txt")[0]
        $clientSecret_enc = (Get-Content "$RootDirectory\Secure\Google_OAuth.txt")[1]
        $refreshToken_enc = (Get-Content "$RootDirectory\Secure\Google_OAuth.txt")[2]

        $clientID = undo-encrypt $clientID_enc
        $clientSecret = undo-encrypt $clientSecret_enc
        $refreshToken = undo-encrypt $refreshToken_enc

        #Checks if one hour passed since last access token refresh
        if (Test-Path -path "$RootDirectory\Secure\Google_AccessToken.txt") {
            $File = Get-ChildItem -Path "$RootDirectory\Secure\Google_AccessToken.txt" | New-Timespan
            #If more than one hour has passed, get new access token
            if ($File.Hours -gt 0) {
                $accessToken = Get-RefreshToken $clientID $clientSecret $refreshToken $RootDirectory
                return $accessToken.access_token
            }
            #If an hour hasn't passed, use existing token
            else {
                $accessToken_enc = Get-Content "$RootDirectory\Secure\Google_AccessToken.txt"
                $accessToken = undo-encrypt $accessToken_enc
                return $accessToken
            }
        }
        #If access token file got deleted somehow, generate it again
        else {
            $accessToken = Get-RefreshToken $clientID $clientSecret $refreshToken $RootDirectory
            return $accessToken.access_token
        }
    }
    #Throw error if secure file doesn't exist
    else {
        throw "No OAuth credentials found! You must run the createGoogleSecret.ps1 script in the Secure folder."
    }
}

#This will make an encrypted string stored in a file usable on the same user and system
function undo-encrypt ($EncryptedString) {
    $SecureString = ConvertTo-SecureString -String $EncryptedString
    $Pointer = [Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecureString)
    $certPswd = [Runtime.InteropServices.Marshal]::PtrToStringAuto($Pointer)
    return $certPswd
}