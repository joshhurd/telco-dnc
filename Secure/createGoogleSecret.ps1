#!/usr/bin/env powershell
##################################
#RUN THIS TO GENERATE A NEW SECRET
##################################

#Modify this value depending on project scopes needed
$scope = 'https://www.googleapis.com/auth/spreadsheets https://mail.google.com/' #This provides GSheet v4 API access

#File used to store the encrypted string
$SecretFile = ".\Google_OAuth.txt"
$AccessTokenFile = ".\Google_AccessToken.txt"

# Secret string
$clientId = Read-Host "Enter ClientID"
$clientSecret = Read-Host "Enter Client Secret"
Read-Host "Press enter to open a browser window and approve Google access"
#URL used to obtain start an OAuth authorization flow
$url = "https://accounts.google.com/o/oauth2/auth?client_id=$clientID&scope=$scope&response_type=code&redirect_uri=http://localhost/oauth2callback&access_type=offline&approval_prompt=force"


#Kick off the default web browser
Write-Host "Launching web browser to authenticate to GDrive..."
Start-Process $url

$accessCode = Read-Host "Enter refresh token returned in browser URL bar (?code=<CODE>&scope)"

#Send credentials using REST API and get refresh token
$headers = @{ 
  "Content-Type" = "application/x-www-form-urlencoded" 
}
$body = @{
  code          = $accessCode
  client_id     = $clientID
  client_secret = $clientSecret
  redirect_uri  = "http://localhost/oauth2callback"
  grant_type    = 'authorization_code'
}
$params = @{
  'Uri'         = 'https://accounts.google.com/o/oauth2/token'
  'ContentType' = 'application/x-www-form-urlencoded'
  'Method'      = 'POST'
  'Headers'     = $headers
  'Body'        = $body
}

try{
  $tokens = Invoke-RestMethod @params
}
catch{
  Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__
  Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
}

Write-Host $tokens | Out-String

# Save the password
ConvertTo-SecureString -String $clientId -AsPlainText -Force | 
  ConvertFrom-SecureString | 
  Out-File $SecretFile -Encoding UTF8

ConvertTo-SecureString -String $clientSecret -AsPlainText -Force | 
  ConvertFrom-SecureString | 
  Add-Content $SecretFile -Encoding UTF8

ConvertTo-SecureString -String $tokens.refresh_token -AsPlainText -Force | 
  ConvertFrom-SecureString | 
  Add-Content $SecretFile -Encoding UTF8

ConvertTo-SecureString -String $tokens.access_token -AsPlainText -Force | 
  ConvertFrom-SecureString | 
  Out-File $AccessTokenFile -Encoding UTF8

Write-Host 'Encrypted Google credentials store created.'