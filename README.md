
# Telecom DNC Manager

This is a headless DNC manager applet that hooks into a Google Form and completes DNC 
additions or removals based on the input.

For troubleshooting assistance, contact Telecom department, or special contacts:

* alan.noble@selectquote.com
* joshua.hurd@selectquote.com

## Author

- [Joshua Hurd](joshua.hurd@selectquote.com)



## Deployment

This project requires a bit of set up to run in headless mode:<br>
Step 1: Download the repo files to a permanent location.<br>
Step 2: Modify config.txt with the Google sheet information.

* sheetID should be the character string found in the URL of the sheet
* sheetName_Entry should be the first sheet "tab"
* sheetName_Completed should be the second sheet "tab"
* See Google Form section for detailed Form creation

Step 3: Open the "Secure" folder and open createGoogleSecret.ps1<br>
Step 4: Insert the Google OAuth credentials for API usage. See Secure section for more details.<br>
Step 5: Run main.ps1 once. On the first run, it will prompt for Five9 credentials and store them.<br>
step 6: Use Windows Task Scheduler or CRON to run applet at desired intervals.

## Secure

Why the "Secure" folder?

Secure folder stores encrypted credentials for future usage. This allows the applet
to access services without storing open passwords or prompting for credentials.

Google access uses OAuth2.0 and requires the following credentials:

* ClientID
* ClientSecret
* RefreshToken

To get or create OAuth credentials, follow these steps:

* Navigate to console.cloud.google.com/apis/credentials
* If none are listed, create a new OAuth 2.0 Client ID for web applications
    * If creating a new user, go to console.cloud.google.com and add new scopes to include GMail and GSheets API
* Copy the ClientID and Client Secret
* Open a new window and navigate to developers.google.com/oauthplayground/
* Click the cogwheel on the top right and ensure the following settings:
    * OAuth flow: Server-Side
    * OAuth endpoints: Google
    * Authorization endpoint: https://accounts.google.com/o/oauth2/v2/auth
    * Token endpoint: https://oauth2.googleapis.com/token
    * Access token location: Authorization header w/ Bearer prefix
    * Access type: Offline
    * Force prompt: Consent Screen
    * Check Use your own OAuth credentials
* Set the scope for this OAuth access to include:
    * https://mail.google.com
    * https://www.googleapis.com/auth/spreadsheets
* Click "Authorize APIs" on the left below the scopes window
* Check "Auto-refresh the token before it expires."
* Copy the Refresh token field

Five9 access is through user credentials. The same username and password a user might use
works for Five9 API access as well. For example:

* john.doe@email.com
* Password1!

## Google Form

To set up a new Google Form follow these steps:

* Navigate to docs.google.com/forms
* Create a new Blank Form
* Create a Multiple Choice question with two choices named "Add or Remove?":
    * Add
    * Remove
* Create a Short Answer text question named "Number to be Changed" with the 
following data validation:
    * Regular expression: Matches: [09]{10} "Must be a number with 10 digits"
* Now Publish the form and navigate to the associated Google Sheet
* Create the following columns, spelling and capitalization matter:
    * Timestamp (This column is created by default)
    * Add or Remove? (This column should be created already)
    * Number to be Changed (This column should be created already)
    * Email Address
    * Approved
    * Denied
    * Denial Reason
    * API Response

## Intended Implementation

This applet was designed to solve the common request for Telecom to add or remove
numbers to the Five9 DNC. This process, while straightforward, can become tedious and time consuming
with multiple requests.

This process was further complicated by the requirement that Compliance approve all DNC
removals before Telecom could complete requests.

The intended purpose of this applet is to expedite and automate, as much as possible,
the process of receiving requests and completing them with as little friction as possible.

The basic flowchart of this process is as follows:

* SelectQuote employee requests addition or removal of number from the DNC list through the Google Form
* Google Form adds request to spreadsheet shared to Compliance
* For "Add" requests, no approval is needed, so this applet will complete this request immediately
* For "Remove" requests, applet will wait until an approval or denial is made
    * If denied, applet will wait for a denial reason to be given before completing
* When completing a step, the applet will email the requester of the status and move the entry to the second tab
